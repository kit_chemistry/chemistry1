/************/
/*GLOBAL VARS*/
/***********/
var sprt = [];
var iceGraph, sprtIG, 
	atomJoin, sprtAJ, 
	gasPic, sprtGP,
	stateChange, sprtSC,
	waterBoil, sprtWB,
	sprtTh,	sprtMOn, 
	leftPart, rightPart, /*elements on the left and right sides (frame-3, water-cycle-task)*/
	pInitial; /*3 elements (frame-4, water-cycle-task-2)*/
	
	sprt[0] = sprtIG;
	sprt[1] = sprtAJ;
	sprt[2] = sprtGP;
	sprt[3] = sprtSC;
	sprt[4] = sprtWB;
	sprt[5] = sprtTh;
	sprt[6] = sprtMOn;

	var audio = [];
	var audioPieces = [];
	var timeouts = [];
	var paused = false;
	var loadPercentage = 0;
	var currentSecond = 0;
	
/************/
/*JQUERY*/
/************/

function AudioPiece(source, start, end)
{
	this.audio = new Audio("audio/" + source + ".mp3");
	this.start = start;
	this.end = end;
}

AudioPiece.prototype.play = function()
{
	var currPiece = this,
		currAudio = this.audio;
	
	currAudio.currentTime = currPiece.start;
	
	currAudio.addEventListener("timeupdate", function(){
		var currTime = Math.round(currAudio.currentTime);
		
		if(currTime == currPiece.end)
			currAudio.pause();
	});
	
	currAudio.play();
}

var calculateBackgroundSize = function(elem, frameNum)
{
	
}

var loadImages = function()
{
	/*var images = [];
	
	//FOR MENU
	images[0] = new Image();
	images[0].src = "pics/title.jpg"; 
	
	//FOR 1st FRAME
	images[1] = new Image();
	images[1].src = "pics/old-man-girl-comix.png";
	
	//FOR 2nd, 3rd, 4rth FRAMES
	images[2] = new Image();
	images[2].src = "pics/water-cycle.jpg";
	
	//FOR 5th FRAME
	images[3] = new Image();
	images[3].src = "pics/medeo.png";
	images[4] = new Image();
	images[4].src = "pics/medeo-evaporation.png";
	images[5] = new Image();
	images[5].src = "pics/cloud-and-rain.png";
	images[6] = new Image();
	images[6].src = "pics/oil.png";
	images[7] = new Image();
	images[7].src = "pics/sugar.png";
	images[8] = new Image();
	images[8].src = "pics/shampoo.png";
	images[9] = new Image();
	images[9].src = "pics/juice.png";	
	images[10] = new Image();
	images[10].src = "pics/juice-animated.gif";
	images[11] = new Image();
	images[11].src = "pics/gas.gif";
		
	//FOR 6th, 7th FRAMES
	images[12] = new Image();
	images[12].src = "pics/cool-graph.jpg";
	images[13] = new Image();
	images[13].src = "pics/heat-graph.jpg";
	
	//FOR 8th FRAME 
	images[14] = new Image();
	images[14].src = "pics/graph-notepad.png";
	
	//FOR 9th FRAME
	images[15] = new Image();
	images[15].src = "pics/molecules.gif";
	images[16] = new Image();
	images[16].src = "pics/last-frame-bg.png";
	images[17] = new Image();
	images[17].src = "pics/final-girl.png";
	
	//FOR 10th FRAME 
	images[18] = new Image();
	images[18].src = "pics/state-change-animate.jpg";
	
	//FOR 11th FRAME  
	images[19] = new Image();
	images[19].src = "pics/gas-structure.png";
	images[20] = new Image();
	images[20].src = "pics/liquid-structure.png";
	images[21] = new Image();
	images[21].src = "pics/solid-structure.png";
	images[22] = new Image();
	images[22].src = "pics/molecular-structure-task.jpg";
	
	//FOR 12th FRAME
	images[23] = new Image();
	images[23].src = "pics/water-boil-animate.jpg";
	
	//FOR 7th FRAME 
	
	var division = 100 / images.length;
	for (var i = 0; i < images.length; i++)
	{	
		images[i].addEventListener("load", function(){
			loadPercentage += division;
			$("#progress-bar").css("width", Math.round(loadPercentage) + "%");
			
			if (loadPercentage > 100)
			{
				loadPercentage = 100;
				$(".slide").css("visibility", "visible");
				$("#loading-progress").css("visibility", "hidden");
				initVisibleFramesSlides();
			}
			
			console.log(loadPercentage + "% is loaded");
		});
	}
	*/
	loadPercentage = 100;
	$(".slide").css("visibility", "visible");
	$("#loading-progress").css("visibility", "hidden");
	initVisibleFramesSlides();
}

var initActiveFrame = function()
{
	switch($(".active-frame").attr("id"))
	{
		case 'old-man-girl-frame':
			removeNavBar();
			oldManGirlInit2();
			break;
		case 'water-cycle-frame':
			removeNavBar();
			animateProcesses();
			break;
		case 'water-cycle-task-frame':
			removeNavBar();
			backToPlaces();
			hexGameInit();
			break;	
		case 'water-cycle-task-2-frame':
			removeNavBar();
			draggabilliesInit();
			break;
		case 'water-cycle-task-3-frame':
			removeNavBar();
			draggabillies2Init();
			break;
		case 'matter-frame':
			removeNavBar();
			teacherAnimationInit();
			break;
		case 'graph-frame':
			graphAnimationInit();
			break;
		case 'quiz-frame':
			audio[0] = new Audio("audio/s7-1.mp3");
			audio[0].play();
			removeNavBar();
			break;
		case 'sketchpad-frame':
			sketchPadInit();
			break;
		case 'molecular-structure-frame':
			molecularStructureInit();
			break;
		case 'matter-state-change-frame':
			matterStateChangeInit();
			break;
		case 'crystalline-structure-frame':
			crystallineStructureInit();
			break;
		case 'heat-video-frame':
			heatVideoInit();
			break;
		case 'match-task-frame':
			matchTaskInit();
			removeNavBar();
			break;
		case 'drag-fill-task-frame':
			dragFillTaskInit();
			break;
	}
}

var allHaveChildren = function(tags) //to check if elements have children
{
	for (var i = 0; i < tags.length; i++)
	{
		if($(tags[i]).children().length === 0)
		{
			return false;
		}
	}
	return true;
}

var allHaveHTML = function(tags) //to check if elements have html
{
	for (var i = 0; i < tags.length; i++)
	{
		if($(tags[i]).html() === "")
		{
			return false;
		}
	}
	return true;
}

var initVisibleFramesSlides = function()
{
	$('.slide').fadeOut(0);
	$('.active-slide').fadeIn();
	
	$('.frame').fadeOut(0);
	$('.active-frame').fadeIn();
}

var jump = function(activeClassName, nextElement) //for changing slides and frames
{
	if(nextElement.length) //IF THERE IS AN ELEMENT TO JUMP ON
	{	
		var current = $("." + activeClassName),
			next = nextElement;
		
		current.removeClass(activeClassName);
		next.addClass(activeClassName);
		
		current.fadeOut(500);
		next.fadeIn(500);	
	}
	for (var i = 0; i < audio.length; i++)
	{
		audio[i].pause();
		audio[i].currentTime = 0;
	}
	for (var i = 0; i < timeouts.length; i ++)
	{
		clearTimeout(timeouts[i]);
	}
	for (var i = 0; i < sprt.length; i ++)
	{
		if(sprt[i])
			sprt[i].pause();
	}
}

var fadeItIn = function($array, index) //WATER CYCLE RECURSIVE ANIMATION
{
	if(!paused)
	{
		if(index < $array.length)
		{
			
			$array.eq(index).fadeIn(500);
			timeouts[index] = setTimeout(function(){fadeItIn($array, index + 1);}, 1000);
		}	
		else
		{
			showNavBar();
		}
	}
}

var animateProcesses = function() //WATER CYCLE
{
	var processDivs = $(".process"),
		playButtons = $("#water-cycle-frame .fa-play");
	console.log("length is: " + processDivs.length);
	//processDivs.fadeOut(0);
	$(".frame .title").fadeOut(0);
	$(".frame .title").fadeIn(3000);
	
	audio[0] = new Audio("audio/s2-1.mp3");
	//audio[0].play();
	
	audioPiece = {};
	audioPiece["condensation-rus"] = new AudioPiece("s2-1", 0, 4);
	audioPiece["condensation-kaz"] = new AudioPiece("s2-1", 0, 4);
	audioPiece["condensation-eng"] = new AudioPiece("s2-1", 0, 4);
	
	audioPiece["evaporation-rus"] = new AudioPiece("s2-1", 4, 8);
	audioPiece["evaporation-kaz"] = new AudioPiece("s2-1", 4, 8);
	audioPiece["evaporation-eng"] = new AudioPiece("s2-1", 4, 8);
	
	audioPiece["freezing-rus"] = new AudioPiece("s2-1", 7, 12);
	audioPiece["freezing-kaz"] = new AudioPiece("s2-1", 7, 12);
	audioPiece["freezing-eng"] = new AudioPiece("s2-1", 7, 12);
	
	audioPiece["boiling-rus"] = new AudioPiece("s2-1", 12, 16);
	audioPiece["boiling-kaz"] = new AudioPiece("s2-1", 12, 16);
	audioPiece["boiling-eng"] = new AudioPiece("s2-1", 12, 16);
	
	audioPiece["cooling-rus"] = new AudioPiece("s2-1", 17, 23);
	audioPiece["cooling-kaz"] = new AudioPiece("s2-1", 17, 23);
	audioPiece["cooling-eng"] = new AudioPiece("s2-1", 17, 23);
	
	audioPiece["heat-rus"] = new AudioPiece("s2-1", 22, 27);
	audioPiece["heat-kaz"] = new AudioPiece("s2-1", 22, 27);
	audioPiece["heat-eng"] = new AudioPiece("s2-1", 22, 27);
	
	audioPiece["temperature-rus"] = new AudioPiece("s2-1", 27, 35);
	audioPiece["temperature-kaz"] = new AudioPiece("s2-1", 27, 35);
	audioPiece["temperature-eng"] = new AudioPiece("s2-1", 27, 35);
	
	var playButtonsListener = function(){
		var parent = $(this).parent();
		audioPiece[parent.attr("id")].play();
	};
	playButtons.off("click", playButtonsListener);
	playButtons.on("click", playButtonsListener);
	
	//setTimeout(function(){fadeItIn(processDivs,0);}, 3000);	
}

var oldManGirlInit2 = function() //COMIX VERSION OF OLD MAN GIRL
{
	var oldManGirlFrame = document.querySelector("#old-man-girl-frame");
	oldManGirlFrame.style.backgroundPosition = "3% 10%";
	oldManGirlFrame.style.backgroundSize = "300%";
	oldManGirlFrame.style.opacity = "1";
	
	audiosNeeded = 5;
	
	audio[0] = new Audio("audio/nature.mp3"),
	audio[1] = new Audio("audio/elevator.mp3"),
	audio[2] = new Audio("audio/s1-1.mp3"), 
	audio[0].play();
	
	for(var i = 0; i < audio.length; i++)
	{
		audio[i].addEventListener("canplaythrough", function(){
			audiosNeeded --;
			if(audiosNeeded === 0)
				audio[0].play();
		});
	}
	//audio[0].play();
	$("#girls-question").fadeOut(0);
	
	var functions = [];
	
	functions[0] = function(){
		audio[0].volume = 0.2;
		oldManGirlFrame.style.backgroundPosition = "3% 80%";
		oldManGirlFrame.style.backgroundSize = "500%";
	};
	
	functions[2] = function(){
		oldManGirlFrame.style.backgroundPosition = "40% 80%";
		oldManGirlFrame.style.backgroundSize = "300%";
	};
		
	functions[5] = function(){
		$("#girls-question").fadeOut(0);
		oldManGirlFrame.style.backgroundPosition = "99% 15%";
		oldManGirlFrame.style.backgroundSize = "450%";
	};
	functions[6] = function(){
		oldManGirlFrame.style.backgroundPosition = "72% 25%";
		oldManGirlFrame.style.backgroundSize = "500%";
	};
	
	audio[0].addEventListener("timeupdate", function(){
		var currTime = Math.round(this.currentTime);
		if(currentSecond !== currTime)
		{
			currentSecond = currTime;
			
			switch(currentSecond)
			{
				case 4: functions[0]();
						break;
				case 5: audio[1].currentTime = 0;
						audio[1].play();
						break;
				case 9: functions[2]();
						break;
				case 13: audio[0].volume = 0.1; 
						audio[2].currentTime = 0;
						audio[2].play();
						$("#girls-question").fadeIn(500);
						break;
				case 16: functions[5]();
						break;
				case 21: functions[6]();
						break;
				case 23: showNavBar();
						break;
			}
		}
	});
}

var gasAnimate = function(index) //
{
	if (index < 9)
	{
		timeouts[0] = setTimeout(function(){
			$bgPosNew = Math.round(index * 100) + "% 0%";
			$bgPosOld = $("#gas").css("background-position");
			$bgURL = $("#gas").css("background-image");
			$("#gas").css("background", $bgURL + " " + $bgPosNew + ", " + $bgURL + " " + $bgPosOld);
			$("#gas").css("background-size", "auto 100%");
			index++;
			gasAnimate(index);
		}, 500);
	}
}

var teacherAnimationInit = function()
{
	var medeo = $("#medeo"),
		iceSkater = $("#ice-skater"),
		cloudAndRain = $("#cloud-and-rain"),
		medeoEvaporation = $("#medeo-evaporation"),
		matter = $(".matter"),
		sugar = $("#sugar"), 
		shampoo = $("#shampoo"),
		juice = $("#juice"),
		juiceAnimated = $("#juice-animated"), 
		oil = $("#oil"), 
		gas = $("#gas"),
		gasSpread = $("#gas-spread");
	
	audio[0] = new Audio("audio/matter.mp3");
	audio[1] = new Audio("audio/rain.mp3");
	audio[2] = new Audio("audio/ice-skating.mp3");
	
	audio[0].addEventListener("timeupdate", function(){
		var currTime = Math.round(this.currentTime);
		if(currentSecond !== currTime)
		{
			currentSecond = currTime;
			
			switch(currentSecond)
			{
				case 12:setTimeout(function(){
						audio[0].pause(); 
						audio[2].play();
						medeo.css("backgroundSize", "400%");
						medeo.css("backgroundPosition", "60% 80%");
						setTimeout(function()
						{
							iceSkater.fadeIn(0);
							iceSkater.css("backgroundPosition", "70% 70%");
						}, 1000);
					}, 200); 
					break;
				case 13: audio[0].pause();
						cloudAndRain.fadeIn(0);
						cloudAndRain.css("opacity", "0.5");
						setTimeout(function(){
							audio[1].play();
						}, 2000)
						break;
				case 14: setTimeout(function(){
							audio[0].pause(); 
						}, 200); 
						medeo.css("backgroundSize", "350%");
						medeo.css("backgroundPosition", "80% 95%");
						medeoEvaporation.css("backgroundSize", "350%");
						medeoEvaporation.css("backgroundPosition", "80% 95%");
						setTimeout(function(){
							medeoEvaporation.fadeIn(300);	
						}, 1000);
						setTimeout(function(){
							audio[0].play();
							matter.fadeOut(1000);
						}, 2000);
						break;
				case 16: sugar.fadeIn(300);
						break;
				case 20: sugar.css("background-position", "-150% 0%"); 
						shampoo.fadeIn(1000);
						break;
				case 22: shampoo.css("background-position", "0% 0%") 
						oil.fadeIn(300);
						break;
				case 23: oil.fadeIn(0);
						break;
				case 24: juice.fadeIn(0);
						juice.css("background-position", "95% 0%");
						break;
				case 25: oil.fadeOut(0);
						 shampoo.fadeOut(0);
						 juice.css("background-position", "20% 0%");
						setTimeout(function(){
							juice.fadeOut(0);
							juiceAnimated.css("background-image", "url('pics/juice-animated.gif')");
							juiceAnimated.fadeIn(0);
						}, 1000);						
						break;
				case 30: juiceAnimated.fadeOut(200);
						gas.fadeIn(0);
						var gasImg = new Image();
						gasImg.src = "pics/gas.gif";
						gasImg.addEventListener("load", function(){
							gas.css("background-image", "url('pics/gas.gif')");
						});
						break;
				case 36: 
						 gas.fadeOut(0);
						 gasSpread.fadeIn(0);
						 var gasSpreadImg = new Image();
						 gasSpreadImg.src = "pics/gas-spread.gif";
						 gasSpreadImg.addEventListener("load", function(){
							gasSpread.css("background-image", "url('pics/gas-spread.gif')")
						 });
						 break;
			}
		}
	});
	audio[0].addEventListener("ended", function(){
		showNavBar();
	});
	
	audio[1].addEventListener("ended", function(){
		audio[0].play();
		cloudAndRain.fadeOut();
	});
	audio[2].addEventListener("ended", function(){
		audio[0].play();
		iceSkater.fadeOut(0);
		medeo.css("backgroundSize", ""); 
		medeo.css("backgroundPosition", "");
	});
	
	var img = new Image();
	audio[0].play();
	matter.fadeOut(0);
	medeo.fadeIn(0);
}

var showHomeOnly = function()
{
	$(".nav .fa-fast-backward, .nav .fa-fast-forward, .nav .fa-repeat").fadeOut(0);
}

var toggleNavBar = function()
{
	if($(".nav").css("background-color") === "transparent")
		showNavBar();
	else
		removeNavBar();
}

var removeNavBar = function()
{
	$(".nav *").fadeOut(0);
	$(".nav .fa-navicon").fadeIn(0);
	$(".nav").css("background", "transparent");
}

var showNavBar = function()
{
	$(".nav *").fadeIn(200);
	$(".nav").css("background", "");
}

var drawMap = function()
{
	var frames = $(".frame");
	var framesNum = frames.length;
	var map = $(".map");
	map.html("");
	var icon = "<i class='icon fa fa-circle'></i>",
		minus = "<i class='fa fa-minus'></i>", 
		home = "<i class='fa fa-home fa-2x'></i>",
		isDone = true;
	var activeIcon = "<i class='active-icon fa fa-circle-o fa-2x'></i>";
	
	map.append(home, minus);
	
	for(var i = 0; i < framesNum; i ++)
	{
		if($(frames[i]).hasClass("active-frame"))
		{
			map.append(activeIcon);
			isDone = false;
		}
		else if(isDone)
		{
			icon = "<i title='"+frames[i].id+"' class='icon fa fa-circle'></i>";
			map.append(icon);
		}
		else
		{
			icon = "<i title='"+frames[i].id+"' class='icon fa fa-circle-o'></i>";
			map.append(icon);
		}
		if(i < framesNum - 1)
			map.append(minus)
	}
	
	$(".map .fa-home").click(function(){
		var next = $("#menu-slide");
		jump("active-slide", next);
	});
	
	$(".icon").click(function(){
		var next = $("#"+$(this).attr("title"));
		jump("active-frame", next);
		drawMap();
		
		initActiveFrame();
	});
}

var backToPlaces = function() //for water-cycle-task-2
{
	$leftPart = $("#process-container span");
	$rightPart = $("#process-container-2 span");
	
	if($leftPart.length === 8 && $rightPart.length === 6)
	{
		
	}
	else
	{
		$("#process-container").html(leftPart);
		$("#process-container-2").html(rightPart);
	}
}

var backToPlaces2 = function() //for water-cycle-task-2
{	
	if($("#water-cycle-task-2-frame").children().length === 10)
	{
		
	}
	else
	{
		$("#water-cycle-task-2-frame").html(pInitial.html());
	}
}

var hexGameInit = function() //hexGameInit
{
	removeNavBar();
	audio[0] = new Audio("audio/water-droplet.mp3");
	audio[1] = new Audio("audio/water-splash.mp3");
	audio[2] = new Audio("audio/wood-cracking.mp3");
	audioPieces[0] = new AudioPiece("s3-1", 0, 8);
	audioPieces[1] = new AudioPiece("s3-1", 11, 20);
	
	$("#process-container-row-1 span, #process-container-row-2 span, #process-container-row-3 span")
	.click(function(){
		audio[2].play();
		$(this).toggleClass("active");
		
		var activeSpans = $(".active");
		var rows = [$("#process-container-row-1"), $("#process-container-row-2"), $("#process-container-row-3")];
		var spanWidth = parseInt($("#process-container-row-2 span").css("width"));
		
		if (activeSpans.length === 3)
		{
			dataProcess = [$(activeSpans[0]).attr("data-process"), $(activeSpans[1]).attr("data-process"),
							$(activeSpans[2]).attr("data-process")];
			if((dataProcess[0] === dataProcess[1]) && (dataProcess[1] === dataProcess[2]))
			{
				audio[0].play();
				activeSpans.addClass("");
				activeSpans.css("opacity", "0");
				activeSpans.removeClass("active");
				setTimeout(function(){
					for(var i = 0; i < activeSpans.length; i ++)
					{
						rowPadding = parseInt($(activeSpans[i]).parent().css("padding-left"));
						rowPadding += spanWidth;
						if($(activeSpans[i]).siblings().length % 2)
							$(activeSpans[i]).parent().css("padding-right", rowPadding+"px");
						else
							$(activeSpans[i]).parent().css("padding-left", rowPadding+"px");
						$(activeSpans[i]).remove();
						if((rows[0].children().length === 0) &&
							(rows[1].children().length === 0) &&
							(rows[2].children().length === 0))
							{
								audioPieces[1].play();
								showNavBar();
							}
					}	
				}, 1000);
			}
			else
			{
				activeSpans.removeClass("active");
				activeSpans.addClass("error");
				audio[1].play();
				setTimeout(function(){activeSpans.removeClass("error");}, 1000);
			}				
		}
	});
	timeouts[0] = setTimeout(function(){
		audioPieces[0].play();
	}, 1000);
}

var draggabilliesInit = function()
{
	removeNavBar();
	
	audio[0] = new Audio("audio/water-droplet.mp3");
	audio[1] = new Audio("audio/water-splash.mp3");
	audio[2] = new Audio("audio/wood-cracking.mp3");
	audio[3] = new Audio("audio/s4-1.mp3");
	
	var draggables = $("#water-cycle-task-2-frame .ball");
	var draggabillies = [];
	
	for(var i = 0; i < draggables.length; i++)
		draggabillies[i] = new Draggabilly(draggables[i]);
	
	var onEnd = function(instance, event, pointer)
	{
		$vegetable = $(event.target);
		$vegetable.css("display", "none");
		
		var basket = document.elementFromPoint(pointer.pageX, pointer.pageY);
		
		if($vegetable.hasClass(basket.id))
		{
			audio[0].play();
			$("."+$vegetable.attr("id")).css("opacity", 0);
			$vegetable.attr("id", $vegetable.attr("id") + "-inside");
			basket.innerHTML = $vegetable[0].innerHTML;
			$vegetable.remove();
		}
		else
		{
			audio[1].play();
			tdrag = new Draggabilly($vegetable[0]);
			tdrag.destroy();
			tdrag = new Draggabilly($vegetable[0]);
			tdrag.on("dragEnd", onEnd);
			tdrag.on("dragStart", onStart);
			$vegetable.css("display", "");
		}
		$vegetable.css("display", "");
		$vegetable.css("z-index", "10");
		event.target.style.border = "";
		
		if(allHaveHTML($("#water-cycle-task-2-frame .basket")))
		{
			alert("поздравляю!!");
			$("#bottom-container").css("opacity", "0");
			showNavBar();
		}
	}
	
	var onStart = function(instance, event, pointer)
	{
		audio[2].play();
		event.target.style.zIndex = "10";
		event.target.style.backgroundColor = "#FF3467";
	}
	
	for(var i = 0; i < draggabillies.length; i++)
	{
		draggabillies[i].on("dragEnd", onEnd);
		draggabillies[i].on("dragStart", onStart);
	}

	timeouts[0] = setTimeout(function(){
		audio[3].play();
	}, 1000);
}

var draggabillies2Init = function()
{
	removeNavBar();
	
	audio[0] = new Audio("audio/water-droplet.mp3");
	audio[1] = new Audio("audio/water-splash.mp3");
	audio[2] = new Audio("audio/wood-cracking.mp3");
	
	var draggables = $("#water-cycle-task-3-frame .ball");
	var draggabillies = [];
	
	for(var i = 0; i < draggables.length; i++)
		draggabillies[i] = new Draggabilly(draggables[i]);
	
	var onEnd = function(instance, event, pointer)
	{
		$vegetable = $(event.target);
		$vegetable.css("display", "none");
		
		var basket = document.elementFromPoint(pointer.pageX, pointer.pageY);
		
		if($vegetable.attr("data-temperature") === $(basket).attr("data-temperature"))
		{
			audio[0].play();
			$("."+$vegetable.attr("id")).css("opacity", 0);
			$vegetable.attr("id", $vegetable.attr("id") + "-inside");
			basket.innerHTML = $vegetable[0].innerHTML;
			$vegetable.remove();
		}
		else
		{
			audio[1].play();
			tdrag = new Draggabilly($vegetable[0]);
			tdrag.destroy();
			tdrag = new Draggabilly($vegetable[0]);
			tdrag.on("dragEnd", onEnd);
			tdrag.on("dragStart", onStart);
			$vegetable.css("display", "");
			$vegetable.css("backgroundColor", "");
		}
		$vegetable.css("display", "");
		$vegetable.css("z-index", "10");
		event.target.style.border = "";
		
		if(allHaveHTML($("#water-cycle-task-3-frame .basket")))
		{
			alert("поздравляю!!");
			$("#water-cycle-task-3-frame .bottom-container").css("opacity", "0");
			showNavBar();
		}
	}
	
	var onStart = function(instance, event, pointer)
	{
		audio[2].play();
		event.target.style.zIndex = "10";
		event.target.style.backgroundColor = "#FF3467";
	}
	
	for(var i = 0; i < draggabillies.length; i++)
	{
		draggabillies[i].on("dragEnd", onEnd);
		draggabillies[i].on("dragStart", onStart);
	}
}

var graphAnimationInit = function()
{
	removeNavBar();
	
	audio[0] = new Audio("audio/s6-1.mp3");
		
	var graphElement = $(".graph-element"),
		graphAnimated = $("#graph-animated");
	
	graphElement.css("opacity", "0");
	
	audio[0].addEventListener("timeupdate", function(){
		var currTime = Math.round(this.currentTime);
		switch(currTime)
		{
			case 18: graphAnimated.css("opacity", "1");
					graphAnimated.css("background-image", "url('pics/solid-to-liquid-graph.gif')");
					break;
			case 28: graphAnimated.css("background-image", "url(pics/liquid-to-gas-graph.gif)");
					break;
			case 33: graphAnimated.css("background-image", "url(pics/gas-to-liquid-graph.gif)");
					break;
			case 38: graphAnimated.css("background-image", "url(pics/liquid-to-solid-graph.gif)");
					break;
		}
	});
	
	audio[0].addEventListener("ended", function(){
		showNavBar();
	});
	
	
	timeouts[0] = setTimeout(function(){
		audio[0].play();
	}, 1000);
}

var sketchPadInit = function()
{
	removeNavBar();
	$sketchpad = $("#responsive-sketchpad");
	
	var sketchpad = $sketchpad.sketchpad(
	{
		aspectRatio: 2/1            // (Required) To preserve the drawing, an aspect ratio must be specified
	});	
	
	sketchpad.setLineColor('#177EE5');
	sketchpad.setLineSize(3);
	
	$(".sketch-control").click(function(){
		switch($(this).attr("id"))
		{
			case "clear":
				sketchpad.clear();
			break;
			case"undo":
				sketchpad.undo();
			break;
			case "red":
				sketchpad.setLineColor('#D5000D');
			break;
			case "blue":
				sketchpad.setLineColor('#177EE5');
			break;
			case "download":
			var canvas = document.getElementById("responsive-sketchpad");
			  var context = canvas.getContext("2d");
			  context.fillStyle = "green";
			  window.open(canvas.toDataURL("image/png"), '_blank');
			  showNavBar();
			break;
		}
	});
}

var molecularStructureInit = function()
{
	$("#atom-join, #ice-zoom, #water-zoom, #gas-zoom").fadeOut(0);
	removeNavBar();
	var atomJoin = $("#atom-join"), 
		iceZoom = $("#ice-zoom"), 
		crystalCageIce = $(".structure-pic");
	
	audio[0] = new Audio("audio/s9-1.mp3");
	
	audio[0].addEventListener("timeupdate", function(){
		var currTime = Math.round(this.currentTime);
		switch(currTime)
		{
			case 3: atomJoin.fadeIn(0);
					break;
			case 8: iceZoom.fadeIn(0);
					crystalCageIce.fadeOut(0);
					break;
			case 9: atomJoin.fadeOut(0);
					break;
			case 10: //iceZoom.css("background-position", "50% 50%");
					iceZoom.animate({
						backgroundSize: "300%",
						backgroundPosition: "50% 50%"} , 2000);
					break;
			case 15: crystalCageIce.fadeIn(0);
					break;
		}
	});
	
	audio[0].addEventListener("ended", function(){
		showNavBar();
	});
	
	img = new Image();
	img.src = "pics/glass.gif";
	img.addEventListener("load", function(){audio[0].play();})
	audio[0].play();
}

var matterStateChangeInit = function()
{
	removeNavBar();
		audio[0] = new Audio("audio/s10-1.mp3"),
		stateChange = document.querySelector("#pic-container");
		sprtSC = new Motio(stateChange, {
			fps: 1,
			frames: 15
		});
	sprtSC.on("frame", function(){
		if(sprtSC.frame === 14)
			sprtSC.pause();
	});
		$elSize = (parseInt($("#pic-container").css("width")) * 15) + "px " + $("#gas").css("height");
		$("#pic-container").css("background-size", $elSize);	
	
	$(".active-frame").css("background-color", "white");
	audio[0].play();
	sprtSC.play();
	
	audio[0].addEventListener("ended", function(){
		showNavBar();
	});
}

var crystallineStructureInit = function()
{
	removeNavBar();
	var draggables = $("#crystalline-structure-frame .structure-ball");
	var draggabillies = [];
	
	audio[0] = new Audio("audio/s11-1.mp3");
	
	for(var i = 0; i < draggables.length; i++)
	{
		draggabillies[i] = new Draggabilly(draggables[i]);
	}
	
	var onEnd = function(instance, event, pointer)
	{
		$vegetable = $(event.target);
		$vegetable.css("display", "none");
		
		var basket = document.elementFromPoint(pointer.pageX, pointer.pageY);
		
		if($(basket).attr("data-key") == $vegetable.attr("data-key"))
		{
			$vegetable.remove();
			$(basket).addClass("hasShadow");
		}
		else
		{
			tdrag = new Draggabilly($vegetable[0]);
			tdrag.destroy();
			tdrag = new Draggabilly($vegetable[0]);
			tdrag.on("dragEnd", onEnd);
			tdrag.on("dragStart", onStart);
			$vegetable.css("display", "");
		}
		$vegetable.css("display", "");
		$vegetable.css("z-index", "1");
		event.target.style.border = "";
		
		if($(".structure-ball").length === 0)
		{
			alert("поздравляю!!");
			showNavBar();
		}
	}
	
	var onStart = function(instance, event, pointer)
	{
		event.target.style.zIndex = "3";
		event.target.style.border = "0.5vmin solid red";
	}
	
	for(var i = 0; i < draggabillies.length; i++)
	{
		draggabillies[i].on("dragEnd", onEnd);
		draggabillies[i].on("dragStart", onStart);
	}
	
	timeouts[0] = setTimeout(function(){
		audio[0].play();
	}, 1000);
}

var heatVideoInit = function()
{
	removeNavBar();
	audio[0] = new Audio("audio/kettle-boil.mp3");
	var thermometer = document.querySelector("#thermometer-animated"), 
		moleculesOn = document.querySelector("#molecules-animated-on"),
		moleculesOff = document.querySelector("#molecules-animated-off"),
		teapotOn = document.querySelector("#teapot-on"),
		teapotOff = document.querySelector("#teapot-off"),
		teapotAnimated = document.querySelector("#teapot-animated"),
		turnOnButton = document.querySelector("#turn-on-button"),
		turnOffButton = document.querySelector("#turn-off-button"),
		allParts = $("#thermometer-animated, #molecules-animated-on, #molecules-animated-off, #teapot-on, #teapot-off, #teapot-animated, #turn-off-button");
	
	allParts.fadeOut(0);
	$(thermometer).fadeIn(0);
	$(teapotOff).fadeIn(0);
	$(moleculesOff).fadeIn(0);
	
	sprtTh = new Motio(thermometer, {
		"fps": "3", 
		"frames": "12"
	});
	sprtTh.on("frame", function(){
		if(sprtTh.frame === sprtTh.frames - 1)
		{
			sprtTh.pause();
			$(teapotOn).fadeOut(0);
			$(teapotAnimated).fadeIn(0);
			$(moleculesOff).fadeOut(0);
			$(moleculesOn).fadeIn(0);
			sprtMOn = new Motio(moleculesOn, {
				"fps": "2", 
				"frames": "14"
			});
			sprtMOn.on("frame", function(){
				if(sprtMOn.frame === sprtMOn.frames - 1)
					sprtMOn.pause();
			});
			$(moleculesOn).css("background-size", (sprtMOn.frames * 100) + "% 100%");
			sprtMOn.play();
		}
	});
	$(thermometer).css("background-size", (sprtTh.frames * 100) + "% 100%");

	$(moleculesOff).css("background-size", "100% 100%");
	
	$(teapotAnimated).css("background-size", "100% 100%");
	
	$(teapotOff).css("background-size", "100% 100%");
	$(teapotOn).css("background-size", "100% 100%");
	
	audio[0].addEventListener("ended", function(){
		sprtTh.on("frame", function(){
			if(sprtTh.frame === 1)
			{
				sprtTh.pause();
				$(teapotAnimated).fadeOut(0);
				$(teapotOff).fadeIn(0);
			}
		});
		sprtTh.play(true);
		showNavBar();
	});
	
	$(turnOnButton).click(function(){
		audio[0].play();
		$(this).fadeOut(0);
		$(teapotOff).fadeOut(0);
		$(teapotOn).fadeIn(0);
		sprtTh.play();
	});
}

var dragFillTaskInit = function()
{
	removeNavBar();
	var draggables = $(".task-ball");
	var draggabillies = [];
	
	audio[0] = new Audio("audio/s13-1.mp3");
	audio[1] = new Audio("audio/s14-1.mp3");
	
	for (var i = 0; i < draggables.length; i ++ )
	{
		draggabillies[i] = new Draggabilly(draggables[i]);
	}
	
	var onEnd = function(instance, event, pointer)
	{
		$vegetable = $(event.target);
		$vegetable.css("display", "none");
		
		var basket = document.elementFromPoint(pointer.pageX, pointer.pageY);
		
		if($vegetable.hasClass(basket.id))
		{
			basket.innerHTML = $vegetable.html();
			$vegetable.remove();
			basket.style.border = "none";
			tdrag = new Draggabilly($vegetable[0]);
			tdrag.destroy();
		}
		else
		{
			tdrag = new Draggabilly($vegetable[0]);
			tdrag.destroy();
			tdrag = new Draggabilly($vegetable[0]);
			tdrag.on("dragEnd", onEnd);
			tdrag.on("dragStart", onStart);
			$vegetable.css("display", "");
		}
		$vegetable.css("display", "");
		$vegetable.css("z-index", "1");
		event.target.style.border = "";
		event.target.style.color = "";
		
		if(!$(".task-ball").length)
		{
			audio[1].play();
			$("#task-ball-container").html("");
			showNavBar();
		}
	}
	
	var onStart = function(instance, event, pointer)
	{
		event.target.style.zIndex = "10";
		event.target.style.color = "#EB022B";
	}
	
	for(var i = 0; i < draggabillies.length; i++)
	{
		draggabillies[i].on("dragEnd", onEnd);
		draggabillies[i].on("dragStart", onStart);
	}

	timeouts[0] = setTimeout(function(){
		audio[0].play();
	}, 1000);
}

var testDraggabilliesInit = function()
{
	$("#test-home").click(function(){
		jump("active-slide", $("#menu-slide"));
	});
	var draggables = $(".question-ball");
	var draggabillies = [];
	
	audio[0] = new Audio("audio/water-droplet.mp3");
	audio[1] = new Audio("audio/water-splash.mp3");
	audio[2] = new Audio("audio/wood-cracking.mp3");
	
	for(var i = 0; i < draggables.length; i++)
		draggabillies[i] = new Draggabilly(draggables[i]);
	
	var onEnd = function(instance, event, pointer)
	{
		$vegetable = $(event.target);
		$vegetable.css("display", "none");
		
		var basket = document.elementFromPoint(pointer.pageX, pointer.pageY);
		
		if($vegetable.attr("data-key") === $(basket).attr("data-key"))
		{
			angular.element(elem).scope().correctAnswerNumber ++;
			audio[0].play();
			$vegetable.remove();
			if($(basket).hasClass("hasShadow"))
				$(basket).addClass("hasTwoShadows");
			else
				$(basket).addClass("hasShadow");
		}
		else
		{
			audio[2].play();
			tdrag = new Draggabilly($vegetable[0]);
			tdrag.destroy();
			tdrag = new Draggabilly($vegetable[0]);
			tdrag.on("dragEnd", onEnd);
			tdrag.on("dragStart", onStart);
			$vegetable.css("display", "");
		}
		$vegetable.css("display", "");
		$vegetable.css("z-index", "1");
		event.target.style.border = "";
		
		event.target.style.backgroundColor = "";
		event.target.style.color = "";
		
		var elem = document.getElementById("test-slide");
		angular.element(elem).scope().questionAnswered = true;
		angular.element(elem).scope().update();
	}
	
	var onStart = function(instance, event, pointer)
	{
		audio[1].play();
		event.target.style.zIndex = "3";
		event.target.style.backgroundColor = "#DD1144";
		event.target.style.color = "white";
	}
	
	for(var i = 0; i < draggabillies.length; i++)
	{
		draggabillies[i].on("dragEnd", onEnd);
		draggabillies[i].on("dragStart", onStart);
	}
}

var navButtonListeners = function() //FRAME NAVIGATION BUTTONS
{
	$('.nav-btn').click(function(){
		var framesChanged = false;
		switch($(this).attr('class'))
		{
			case 'nav-btn fa fa-fast-forward':
				jump('active-frame', $(".active-frame").next());
				drawMap();
				framesChanged = true;
				break;
			case 'nav-btn fa fa-fast-backward':
				jump('active-frame', $(".active-frame").prev());
				drawMap();
				framesChanged = true;
				break;
			case 'nav-btn fa fa-repeat':
				framesChanged = true;
				break;
			case 'nav-btn fa fa-home':
				jump("active-slide", $("#menu-slide"));
				break;
			case 'nav-btn fa fa-navicon':
				toggleNavBar();
				break;
		}	
		
		if (framesChanged)
		{
			initActiveFrame();
		}
	});
}

var menuButtonListeners = function() //MAIN MENU NAV BUTTONS
{
	$('.menu-btn').click(function(){
		var current = $(this).parent(),
			next;
		switch($(this).attr('id'))
		{
			case 'der-btn': 
				next = $("#der-slide");
				break;
			case 'facts-btn':
				next = $("#facts-slide");
				break;
			case 'test-btn':
				next = $("#test-slide");
				angular.element(document.getElementById("test-slide")).scope().setInitialTime();  
				angular.element(document.getElementById("test-slide")).scope().setCurrentTime();  
				break;
			case 'research-btn':
				next = $("#research-slide");
				break;
		}
		
		jump('active-slide', next);
				
		if (next.attr("id") === "der-slide")
		initActiveFrame();
	});
}

var matchTaskInit = function() //MATCH TASK FRAME
{
	removeNavBar();
	audio[0] = new Audio("audio/water-droplet.mp3");
	audio[1] = new Audio("audio/water-splash.mp3");
	audio[2] = new Audio("audio/wood-cracking.mp3");
	
	var draggables = $("#match-task-frame .ball .handle");
	var draggabillies = [];
	
	for(var i = 0; i < draggables.length; i++)
	{	
		draggabillies[i] = new Draggabilly(draggables[i]);
	}
	
	var onEnd = function(instance, event, pointer)
	{
		$vegetable = $(event.target);
		$vegetable.css("display", "none");
		
		var basket = document.elementFromPoint(pointer.pageX, pointer.pageY);
		
		if($vegetable.attr("data-key") === $(basket).attr("data-key"))
		{
			audio[0].play();
			$("."+$vegetable.attr("id")).css("opacity", 0);
			$(basket).css("backgroundColor", "#84C642");
			$vegetable.remove();
		}
		else
		{
			audio[1].play();
			tdrag = new Draggabilly($vegetable[0]);
			tdrag.destroy();
			tdrag = new Draggabilly($vegetable[0]);
			tdrag.on("dragEnd", onEnd);
			tdrag.on("dragStart", onStart);
			$vegetable.css("display", "");
		}
		$vegetable.css("display", "");
		$vegetable.css("z-index", "10");
		event.target.style.border = "";
		
		if($(".handle").length === 0)
		{
			alert("поздравляю!!");
			$("#bottom-container").css("opacity", "0");
			showNavBar();
		}
		$vegetable.css("backgroundColor", "");
	}
	
	var onStart = function(instance, event, pointer)
	{
		audio[2].play();
		event.target.style.zIndex = "10";
		event.target.style.backgroundColor = "#FF3467";
	}
	
	for(var i = 0; i < draggabillies.length; i++)
	{
		draggabillies[i].on("dragEnd", onEnd);
		draggabillies[i].on("dragStart", onStart);
	}
}

var factsInit = function()
{
	var factsText = $("#facts-slide .fact-text"),
		sourcePic = $("#source-pic"),
		factsPic = $(".fact-pic");
	
	for(var i = 0; i < factsPic.length; i++)
	{
		if($(factsPic[i]).attr("id") === "")
			factsPic[i].remove();
	}
	
	factsPic.click(function(){
		alert($(this).attr("class"));
	});
	factsText.fadeOut(0);
	sourcePic.fadeOut(0);
}

var main = function(){
	loadImages();
	
	menuButtonListeners();
	navButtonListeners();
	testDraggabilliesInit();
	drawMap();
	factsInit();
	animateProcesses();
	//dragFillTaskInit();
	//matchTaskInit();
	//molecularStructureInit();
};

$(document).ready(main);

/************/
/*ANGULAR*/
/************/

var myApp = angular.module('myApp', ['ngSanitize']);

myApp.controller('myCtrl', function($scope, $http, $timeout){
	
	$scope.questions = {};
	$http.get("json/questions.json").success(
	function(response){
		$scope.questions = response;
	}).error(function(response){
		alert("error: " + response);
	});
	
	$scope.activeQuestion = 1;
	$scope.corrects = 0;
	
	$scope.goFurther = function()
	{
		if($scope.questions[$scope.activeQuestion - 1].answer == $scope.questions[$scope.activeQuestion - 1].useranswer)
			$scope.corrects++;
		
		$scope.activeQuestion++;
		if($scope.activeQuestion == 4)
			showNavBar();
	};
	
	$scope.answerPick = function(answer)
	{
		if ($scope.activeQuestion !== 3)
			$scope.questions[$scope.activeQuestion - 1].useranswer = parseInt(answer);
		else
			$scope.questions[$scope.activeQuestion - 1].useranswer = answer;
		$scope.goFurther();
	}
	
	$scope.matchAnswer = [];
	$scope.correctAnswer = {};
	$scope.correctAnswerNumber = 0;
	$http.get("json/matchanswers.json").success(function(response){
		$scope.correctAnswer = response;
	}).error(function(response){
		alert("error: " + reponse);
	});
	
	$scope.checkMatchAnswers = function()
	{
		$scope.correctAnswerNumber = 0;
		if($scope.correctAnswer[0].indexOf($scope.matchAnswer[0]) != -1)
			$scope.correctAnswerNumber++;
		if($scope.correctAnswer[1].indexOf($scope.matchAnswer[1]) != -1)
			$scope.correctAnswerNumber++;
		
		alert("У вас " + $scope.correctAnswerNumber + " правильных ответов");
		showNavBar();
	}
	
	/*FACTS PART*/
	$scope.facts = {};
	
	$http.get("json/facts.json").success(function(response){
		$scope.facts = response;
	}).error(function(response){
		alert("error: " + response);
	});
	$scope.done = false;
	$scope.initF = function(last){
		if(!$scope.done)
		{
			factsInit();
			$scope.done = true;
		}
	};
	
	/*RESEARCH PART*/
	$scope.research = {};
	
	$http.get("json/research.json").success(function(response){
		$scope.research = response;
	}).error(function(response){
		alert("error: " + response);
	});
	
	/*TEST PART*/
	$scope.test1 = {};
	$scope.questionNumber = 0; //Don't forget to change back
	$scope.correctAnswerNumber = 0;
	$scope.questionAnswered = false;
	$scope.timeNeeded = 5; //in minutes
	$scope.now = new Date();
	$scope.time = $scope.now.getTime();
	$scope.timeFinal = $scope.time + ($scope.timeNeeded * 60000);
	$scope.timeLeft = ($scope.timeFinal - $scope.time ) / 60000;
	
	$http.get("json/test-1.json").success(function(response){
		$scope.test1 = response;
	}).error(function(response){
		alert("error: " + response);
	});
	
	$scope.checkIt = function(index, correct)
	{
		var answer = $(".test-question-"+$scope.questionNumber+"-answer-"+index), 
			answerDrop = $(".test-question-"+$scope.questionNumber+"-answer-"+index+" .rain-drop"),
			grandpa = $("#grandpa");
		
		if(!$scope.questionAnswered)
		{	
			if (index === correct)
			{
				answerDrop.css("background-image", "url(pics/vapor.png)");
				answer.css({
					zIndex: "10",
					width: "100%",
					height: "18%",
					top: "-90%"
				});
				grandpa.css("background-image", "url(pics/grandpa-happy.gif)");
				$timeout(function(){answer.remove();$scope.nextQuestion();}, 3000);
				$scope.correctAnswerNumber ++;
			}
			else
			{
				answerDrop.css("background-image", "url(pics/snowflake.png)");
				answer.css({
					zIndex: "10",
					width: "100%",
					height: "18%",
					top: "100%"
				});
				grandpa.css("background-image", "url(pics/grandpa-angry.gif)");
				$timeout(function(){answer.remove();$scope.nextQuestion();}, 3000);
			}
			$scope.questionAnswered = true;
		}
	}
	
	$scope.initD = function(index)
	{
		if(index === $scope.test1.questions.length - 1)
			testDraggabilliesInit();
	}
	
	$scope.update = function()
	{
		$scope.nextQuestion();
		$scope.$apply();
	}
	
	$scope.nextQuestion = function()
	{
		$scope.questionNumber ++;
		$scope.questionAnswered = false;
		$("#grandpa").css("background-image", "");
		
		if($scope.questionNumber == $scope.test1.questions.length)
		{
			$(".timer").html("");
			$scope.timeLeft = 0;
		}
	}
	
	$scope.setInitialTime = function()
	{
		$scope.now = new Date();
		$scope.time = $scope.now.getTime();
		$scope.timeFinal = $scope.time + ($scope.timeNeeded * 60000);
		$scope.timeLeft = ($scope.timeFinal - $scope.time ) / 60000;
	}
	
	$scope.setCurrentTime = function()
	{
		$scope.now = new Date();
		$scope.time = $scope.now.getTime();
		$scope.timeLeft = Math.round(($scope.timeFinal - $scope.time ) / 1000);
				
		if($scope.timeLeft <= 0)
		{
			$(".timer").html("");
			alert("Время Вышло");
			$scope.questionNumber = $scope.test1.questions.length;			
		}
		else
		{
			$timeout(function(){$scope.setCurrentTime();}, 1000);
		}
	}
});


